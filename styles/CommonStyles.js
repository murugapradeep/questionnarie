import PlatformStyleSheet from "./PlatformStyleSheet";
export const navBarStyle = PlatformStyleSheet.create({
  container: {
    flex: 1
  }
});
