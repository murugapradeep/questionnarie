import PlatformStyleSheet from "./PlatformStyleSheet";
import Theme from "../helpers/Theme";
import { window } from "../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;

export default (SplashViewStyles = PlatformStyleSheet.create({
  container: {
    width: DeviceWidth,
    height: DeviceHeight,
    backgroundColor: Theme.background,
    flexDirection: "column"
  },
  instructionView: {
    width: DeviceWidth - 20,
    height: 100,
    marginTop: 30,
    marginLeft: 10,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  instruction: {
    width: DeviceWidth - 20,
    fontSize: 20,
    textAlign: "center",
    alignItems: "center",
    color: Theme.title,
    height: 70,
    alignItems: "center",
    justifyContent: "center"
  },
  scrlView: {
    flex: 1,
    marginBottom: 0
  }
}));
