import PlatformStyleSheet from "./PlatformStyleSheet";
import Theme from "../helpers/Theme";
import { window } from "../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
export default (QuestionViewStyles = PlatformStyleSheet.create({
  container: {
    width: DeviceWidth,
    height: DeviceHeight,
    flexDirection: "column",
    backgroundColor: Theme.background
  },
  answerView: {
    flex: 1,
    paddingTop: 20,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  scrlView: {
    flex: 1,
    marginBottom: 0
  },
  sliderValue: {
    width: 64,
    height: 40,
    fontSize: 34,
    letterSpacing: -0.43,
    textAlign: "left",
    color: Theme.title,
    position: "absolute",
    top: 60,
    left: 40
  },
  sliderLabel: {
    width: DeviceWidth / 2 - 30,
    fontSize: 17,
    textAlign: "left",
    color: Theme.slider.label
  },
  sliderLabelView:{
    position: "absolute",
    top: 60,
    right: 0,
    width: DeviceWidth / 2 - 30,
    height: DeviceHeight - 300
  }
}));
