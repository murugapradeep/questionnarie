import PlatformStyleSheet from "./PlatformStyleSheet";
import Theme from "../helpers/Theme";
import { window } from "../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
export default (DashboardStyles = PlatformStyleSheet.create({
  container: {
    width: DeviceWidth,
    height: DeviceHeight,
    flexDirection: "column",
    backgroundColor: Theme.greyBackground
  },
  answerView: {
    flex: 1,
    paddingTop: 20,
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: Theme.greyBackground
  },
  titleView: {
    width: DeviceWidth,
    height: 80,
    paddingTop: 40,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },
  title: {
    fontSize: 15,
    padding: 10,
    lineHeight: 21,
    letterSpacing: -0.41,
    textAlign: "center",
    color: "#4a4a4a",
    backgroundColor: "transparent"
  },
  scrlView: {
    flex: 1,
    marginBottom: 0
  }
}));
