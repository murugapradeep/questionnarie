import PlatformStyleSheet from "./PlatformStyleSheet";
import Theme from "../helpers/Theme";
import { window } from "../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
export default (FinishViewStyles = PlatformStyleSheet.create({
  container: {
    width: DeviceWidth,
    flexDirection: "column",
    height: DeviceHeight,
    backgroundColor: Theme.background
  },
  instructionView: {
    width: DeviceWidth - 20,
    height: 320,
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  instruction: {
    width: DeviceWidth - 20,
    fontSize: 20,
    textAlign: "center",
    alignItems: "center",
    color: Theme.title,
    height: 70,
    alignItems: "center",
    justifyContent: "center"
  },
  messageTitle: {
    width: DeviceWidth - 20,
    height: 30,
    fontSize: 28,
    fontWeight: "600",
    lineHeight: 30,
    textAlign: "center",
    color: Theme.title
  },
  messageBody: {
    width: DeviceWidth - 40,
    marginTop: 10,
    fontSize: 19,
    lineHeight: 25,
    textAlign: "center",
    color: Theme.title
  },
  closeView: {
    position: "absolute",
    top: 20,
    right: 20,
    zIndex: 1000,
    width: 30,
    height: 30
  },
  closeIcon: {
    width: 30,
    height: 30
  }
}));
