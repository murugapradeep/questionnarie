import QWelcomeView from "./views/SplashView";
import QWelcomeBackView from "./views/WelcomeBackView";
import QTextInput from "./views/TextInputView";
import QMultiText from "./views/MultiTextView";
import QRadioButton from "./views/SingleSelectView";
import QCheckbox from "./views/MultiSelectView";
import QGeneralOption from "./views/GeneralOptionView";
import QSlider from "./views/SliderView";
import QThankYouView from "./views/FinishView";
import QHeader from "./components/Header";
import QPageControl from "./components/PageControl";
import QNavBar from "./components/NavBar";
import QDashboardView from "./views/DashboardView";
import QDashboardRow from "./components/DashboardRow";
import QProgressBar from "./components/ProgressBar";
export {
  QDashboardView,
  QWelcomeView,
  QWelcomeBackView,
  QTextInput,
  QMultiText,
  QRadioButton,
  QCheckbox,
  QGeneralOption,
  QSlider,
  QThankYouView,
  QHeader,
  QPageControl,
  QNavBar,
  QDashboardRow,
  QProgressBar
};

export default QWelcomeView;
