import {Dimensions} from "react-native";
export const device = Dimensions.get("window");
export const window = {
  DeviceWidth: device.width,
  DeviceHeight: device.height
};
