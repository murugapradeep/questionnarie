export default (colors = {
  lightGrey: "#eaeaea",
  silver: "#c6cbd4",
  greyish: "#a4a4a4",
  paleGrey: "#eaecef",
  black: "#000000",
  white: "#ffffff",
  grey: "#9b9b9b"
});
