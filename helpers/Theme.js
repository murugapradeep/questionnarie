import colors from "./Colors";

export default (Theme = {
  background: colors.white,
  greyBackground: colors.paleGrey,
  title: colors.black,
  subTitle: colors.greyish,
  errorText: colors.black,
  headerText: colors.black,
  cancelButtonTitle: colors.grey,
  buttonTitle: colors.black,
  option:{
    unSelectedText: colors.greyish,
    selectedText: colors.black
  },
  header: {
    background: colors.grey,
    title: colors.greyish,
    tint: colors.greyish
  },
  button: {
    title: colors.white,
    border: colors.black,
    background: colors.black,
    unselectedBackground: colors.black,
  },
  textInput: {
    text: colors.black,
    placeholder: colors.grey,
    background: colors.white,
    border: colors.greyish
  },
  slider:{
    highlight: colors.black,
    background: colors.silver,
    label: colors.greyish
  }
});
