import React, { Component } from "react";
import { Slider } from "react-native";
import styles from "./styles";
import Theme from "../../helpers/Theme";

export default class VerticalSlider extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: this.props.value,
      reverseValue: 8
    };
  }

  handleChange = value => {
    this.setState({
      value: value
    });
  };

  handleChangeReverse = value => {
    this.setState({
      reverseValue: value
    });
  };

  render() {
    const { value, reverseValue } = this.state;
    return (
      <Slider
        style={styles.slider}
        step={1}
        maximumTrackTintColor={Theme.slider.background}
        minimumTrackTintColor={Theme.slider.highlight}
        minimumValue={0}
        maximumValue={100}
        value={this.state.value}
        onValueChange={this.props.onValueChange}
        {...this.props}
      />
    );
  }
}
