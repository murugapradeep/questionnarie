import { StyleSheet, Dimensions } from "react-native";
import PlatformStyleSheet from "../../styles/PlatformStyleSheet";
import { window } from "../../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
export default PlatformStyleSheet.create({
  slider: {
    marginTop: 40,
    width: DeviceHeight - 300,
    height: DeviceHeight - 300,
    transform: [{ rotate: "270deg" }]
  }
});
