import React, { Component } from "react";
import { ScrollView } from "react-native";
import styles from "./styles";
import PropTypes from "prop-types";

export default class ScrollViewWrapper extends Component {
  constructor(props: Object) {
    super(props);
  }
  componentDidMount = () => {
    //console.log(Array.isArray(this.props.children)); // => false
    // warning: yields 5 for length of the string 'hello', not 1 for the
    // length of the non-existent array wrapper!
    //console.log(this.props.children.length);
  };
  render() {
    return (
      <ScrollView
        bounces={false}
        scrollEnabled={true}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        alwaysBounceHorizontal={false}
        alwaysBounceVertical={false}
        automaticallyAdjustContentInsets={false}
      >
        {this.props.children}
      </ScrollView>
    );
  }
}
