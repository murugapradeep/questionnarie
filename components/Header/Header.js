import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import styles from "./styles";
import LinearGradient from "react-native-linear-gradient";
import PropTypes from "prop-types";
import NavBar from "../NavBar";

const propTypes = {
  pageNo: PropTypes.number,
  totalPages: PropTypes.number,
  leftButtonTitle: PropTypes.string,
  rightButtonTitle: PropTypes.string,
  leftButtonHandler: PropTypes.func,
  rightButtonHandler: PropTypes.func,
  leftButtonStyle: View.propTypes.style,
  leftButtonTextStyle: Text.propTypes.style,
  rightButtonStyle: View.propTypes.style,
  rightButtonTextStyle: Text.propTypes.style,
  style: View.propTypes.style,
  isNavBarHidden: PropTypes.bool,
  titleViewStyle: View.propTypes.style,
  titleStyle: Text.propTypes.style,
  question: PropTypes.string,
  headerStyle: View.propTypes.style,
  headerBackgroundColors: PropTypes.array,
};

const defaultProps = {
  pageNo: 0,
  totalPages: 7,
  leftButtonTitle: "",
  rightButtonTitle: "",
  leftButtonHandler: () => {},
  rightButtonHandler: () => {},
  leftButtonStyle: {},
  leftButtonTextStyle: {},
  rightButtonStyle: {},
  rightButtonTextStyle: {},
  style: {},
  isNavBarHidden: false,
  question: "",
  titleViewStyle: {},
  titleStyle: {},
  headerStyle: {},
  headerBackgroundColors: [
    "rgb(247,247,247)",
    "rgb(240,240,240)",
    "rgb(225,225,225)"
  ]
};

export default class Header extends Component {
  constructor(props: Object) {
    super(props);
  }
  renderHeader = () => {
    if (this.props.isNavBarHidden) {
      return (
        <View style={[styles.header,this.props.headerStyle]}>
          <LinearGradient
            colors={this.props.headerBackgroundColors}
            style={styles.arcView}
          />
        </View>
      );
    } else {
      return (
        <View style={[styles.headerContainer,this.props.headerStyle]}>
          <View style={styles.bgView}>
            <LinearGradient
              colors={this.props.headerBackgroundColors}
              style={styles.arcView}
            />
          </View>
          <View style={styles.topView}>
            <NavBar
              pageNo={this.props.pageNo}
              totalPages={this.props.totalPages}
              leftButtonTitle={this.props.leftButtonTitle}
              rightButtonTitle={this.props.rightButtonTitle}
              leftButtonHandler={this.props.leftButtonHandler}
              rightButtonHandler={this.props.rightButtonHandler}
              leftButtonStyle={this.props.leftButtonStyle}
              leftButtonTextStyle={this.props.leftButtonTextStyle}
              rightButtonStyle={this.props.rightButtonStyle}
              rightButtonTextStyle={this.props.rightButtonTextStyle}
              style={this.props.style}
            />
            <View style={[styles.QuestionView,this.props.titleViewStyle]}>
              <Text style={[styles.QuestionText,this.props.titleStyle]}>{this.props.question}</Text>
            </View>
          </View>
        </View>
      );
    }
  };
  render() {
    return this.renderHeader();
  }
}

Header.propTypes = propTypes;
Header.defaultProps = defaultProps;
