import { StyleSheet, Dimensions } from "react-native";
import PlatformStyleSheet from "../../styles/PlatformStyleSheet";
import { window } from "../../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
import Theme from "../../helpers/Theme";
export default PlatformStyleSheet.create({
  headerContainer:{
    width: DeviceWidth,
    height: 220,
  },
  bgView: {
    height: 220,
    width: DeviceWidth,
    overflow: 'hidden',
    position: "absolute",
    top: 0
  },
  header: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    overflow: 'hidden',
  },
  topView: {
    width: DeviceWidth,
    flexDirection: "column",
    height: 200,
  },

  arcView: {
    position: "absolute",
    width: DeviceWidth,
    height: DeviceWidth*2,
    borderRadius: DeviceWidth*2,
    bottom: 0,
    backgroundColor: "transparent",
    overflow: 'hidden',
    transform: [{ scaleX: 2 }]
  },
  QuestionView: {
    flex: 1,
    padding: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },
  QuestionText: {
    width: DeviceWidth - 40,
    height: 140,
    fontSize: 17,
    lineHeight: 28,
    letterSpacing: -0.41,
    textAlign: "center",
    color: Theme.title
  },
});
