import { StyleSheet, Dimensions } from "react-native";
import PlatformStyleSheet from "../../styles/PlatformStyleSheet";
import { window } from "../../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
import Theme from "../../helpers/Theme";
export default PlatformStyleSheet.create({
  button: {
    margin: 15,
    marginBottom: 60,
    width: DeviceWidth - 30,
    height: 59,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Theme.button.background
  },
  buttonText: {
    width: 50,
    height: 22,
    fontSize: 17,
    fontWeight: "bold",
    lineHeight: 22,
    textAlign: "center",
    color: Theme.button.title
  },
  row: {
    width: DeviceWidth,
    height: 70,
    marginBottom: 2,
    backgroundColor: Theme.background,
    flexDirection: "column"
  },
  topView: {
    width: DeviceWidth,
    height: 35,
    padding: 5
  },
  title: {
    width: DeviceWidth,
    height: 35,
    fontSize: 17,
    fontWeight: "bold",
    lineHeight: 22,
    textAlign: "left",
    color: Theme.title
  },
  bottomView: {
    width: DeviceWidth,
    height: 35,
    flexDirection: "row",
    padding: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  date: {
    flex: 1,
    fontSize: 15,
    letterSpacing: -0.36,
    fontWeight: "normal",
    lineHeight: 22,
    textAlign: "left",
    color: Theme.subTitle
  },
  stausBarView: {
    width: 120,
    alignItems: "center",
    justifyContent: "center"
  },
  status: {
    paddingLeft: 10,
    flex: 1,
    fontSize: 15,
    letterSpacing: 3,
    fontWeight: "normal",
    lineHeight: 22,
    textAlign: "left",
    color: Theme.subTitle
  },
  arrowIcon: {
    width: 20,
    height: 20,
    position: "absolute",
    right: 5,
    top: 30
  }
});
