import React, { Component } from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import styles from "./styles";
import PropTypes from "prop-types";
import ProgressBar from "../ProgressBar";

const propTypes = {
  title: PropTypes.string,
  date: PropTypes.string,
  status: PropTypes.string,
  onPress: PropTypes.func,
  style: View.propTypes.style,
  titleStyle: Text.propTypes.style,
  dateStyle: Text.propTypes.style,
  statusStyle: Text.propTypes.style,
  stausBarView: View.propTypes.style,
  stausBarBackground: View.propTypes.style,
  stausBarHighlight: View.propTypes.style,
  rowStyle: View.propTypes.style,
  topView: View.propTypes.style,
  bottomView: View.propTypes.style,
  percent: PropTypes.number,
  arrowIconStyle: Image.propTypes.style,
  arrowIcon: Image.propTypes.source
};

const defaultProps = {
  title: "",
  onPress: () => {},
  percent: "",
  date: "",
  status: "",
  style: {},
  titleStyle: {},
  dateStyle: {},
  statusStyle: {},
  stausBarView: {},
  stausBarBackground: {},
  stausBarHighlight: {},
  rowStyle: {},
  topView: {},
  bottomView: {},
  arrowIconStyle: {},
  arrowIcon: require("../../images/right_arrow.png")
};

export default class DashboardRow extends Component {
  constructor(props: Object) {
    super(props);
  }
  render() {
    return (
      <TouchableOpacity
        style={[styles.row, this.props.rowStyle]}
        onPress={this.props.onPress}
      >
        <View style={[styles.topView, this.props.topView]}>
          <Text style={[styles.title, this.props.titleStyle]}>
            {this.props.title}
          </Text>
        </View>
        <View style={[styles.bottomView, this.props.bottomView]}>
          <Text style={[styles.date, this.props.dateStyle]}>
            {this.props.date}
          </Text>
          <View style={[styles.stausBarView, this.props.stausBarView]}>
            <ProgressBar
              backgroundStyle={this.props.stausBarBackground}
              highlightStyle={this.props.stausBarHighlight}
              percent={this.props.percent}
            />
          </View>
          <Text style={[styles.status, this.props.statusStyle]}>
            {this.props.status}
          </Text>
        </View>
        <Image
          style={[styles.arrowIcon, this.props.arrowIconStyle]}
          source={this.props.arrowIcon}
        />
      </TouchableOpacity>
    );
  }
}

DashboardRow.propTypes = propTypes;
DashboardRow.defaultProps = defaultProps;
