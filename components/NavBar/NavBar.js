import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import styles from "./styles";
import PageControl from "../PageControl";
import NavBarButton from "../NavBarButton";
import PropTypes from "prop-types";

const propTypes = {
  pageNo: PropTypes.number,
  totalPages: PropTypes.number,
  leftButtonTitle: PropTypes.string,
  rightButtonTitle: PropTypes.string,
  leftButtonHandler: PropTypes.func,
  rightButtonHandler: PropTypes.func,
  leftButtonStyle: View.propTypes.style,
  leftButtonTextStyle: Text.propTypes.style,
  rightButtonStyle: View.propTypes.style,
  rightButtonTextStyle: Text.propTypes.style,
  style: View.propTypes.style,
};

const defaultProps = {
  pageNo: 0,
  totalPages: 7,
  leftButtonTitle: "",
  rightButtonTitle: "",
  leftButtonHandler: () => {},
  rightButtonHandler: () => {},
  leftButtonStyle: {},
  leftButtonTextStyle: {},
  rightButtonStyle: {},
  rightButtonTextStyle: {},
  style: {},
};
export default class NavBar extends Component {
  constructor(props: Object) {
    super(props);
  }
  render() {
    return (
      <View style={styles.navbar}>
        <NavBarButton
          style={[styles.leftView,this.props.leftButtonStyle]}
          onPress={this.props.leftButtonHandler}
          title={this.props.leftButtonTitle}
          titleStyle={[styles.leftText,this.props.leftButtonTextStyle]}
        />
        <View style={styles.middleView}>
          <PageControl selIndex={this.props.pageNo} totalPages={this.props.totalPages} />
        </View>
        <NavBarButton
          style={[styles.rightView,this.props.rightButtonStyle]}
          onPress={this.props.rightButtonHandler}
          title={this.props.rightButtonTitle}
          titleStyle={[styles.rightText,this.props.rightButtonTextStyle]}
        />
      </View>
    );
  }
}

NavBar.propTypes = propTypes;
NavBar.defaultProps = defaultProps;
