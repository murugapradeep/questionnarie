import { StyleSheet, Dimensions } from "react-native";
import PlatformStyleSheet from "../../styles/PlatformStyleSheet";
import { window } from "../../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
import Theme from "../../helpers/Theme";
export default PlatformStyleSheet.create({
  navbar: {
    flexDirection: "row",
    width: DeviceWidth,
    height: 50,
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },
  leftView: {
    width: 80,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },
  leftText: {
    fontSize: 17,
    fontWeight: "normal",
    lineHeight: 22,
    height: 20,
    textAlign: "left",
    color: Theme.cancelButtonTitle
  },
  middleView: {
    flex: 1,
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "center",
  },
  middleText: {
    flex: 1,
    color: Theme.buttonTitle,
    fontSize: 14,
    fontWeight: "normal",
    textAlign: "left"
  },
  rightView: {
    width: 80,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },
  rightText: {
    fontSize: 19,
    fontWeight: "500",
    textAlign: "right",
    color: Theme.buttonTitle
  },
});
