import React, { Component } from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import styles from "./styles";
import PropTypes from "prop-types";

const propTypes = {
  image: Image.propTypes.source.isRequired,
  title: PropTypes.string.isRequired,
  iconType: PropTypes.string,
  onPress: PropTypes.func.isRequired,
  isSelected: PropTypes.bool,
  textStyle: Text.propTypes.style,
  selectedTextStyle: Text.propTypes.style,
  iconStyle: Image.propTypes.style
};

const defaultProps = {
  title: "",
  iconType: "Image",
  onPress: () => {},
  isSelected: false,
  textStyle: {},
  selectedTextStyle: {}
};

export default class Checkbox extends Component {
  constructor(props: Object) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity style={styles.row} onPress={this.props.onPress}>
        <Image style={styles.icon} source={this.props.image} />
        <Text
          style={
            this.props.isSelected === false
              ? [styles.title, this.props.textStyle]
              : [
                  styles.title,
                  styles.selectedTitle,
                  this.props.selectedTextStyle
                ]
          }
        >
          {this.props.title}
        </Text>
      </TouchableOpacity>
    );
  }
}
Checkbox.propTypes = propTypes;
Checkbox.defaultProps = defaultProps;
