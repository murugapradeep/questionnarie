import React, { Component } from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import styles from "./styles";
import RadioButton from "../RadioButton";
import PropTypes from "prop-types";
const propTypes = {
  data: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  textStyle: Text.propTypes.style,
  selectedTextStyle: Text.propTypes.style,
};

const defaultProps = {
  data: [],
  onChange: () => {},
  textStyle: {},
  selectedTextStyle: {},
};
export default class RadioButtonGroup extends Component {
  constructor(props: Object) {
    super(props);
    this.state = {
      selectedIndex: -1
    };
    this.data = props.data;
  }

  componentDidMount = () => {};

  _handleOnPress = selectedValue => {
    this.setState({
      selectedIndex: selectedValue
    });
    this.props.onChange(selectedValue);

  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.data.map((option, i) => (
          <RadioButton
            key={i}
            image={
              this.state.selectedIndex == i
                ? option.selectedIcon
                : option.unSelectedIcon
            }
            iconType="image"
            title={option.title}
            isSelected={this.state.selectedIndex == i ? true : false}
            onPress={this._handleOnPress.bind(this, i)}
            textStyle={this.props.textStyle}
            selectedTextStyle={this.props.selectedTextStyle}
          />
        ))}
      </View>
    );
  }
}
RadioButtonGroup.propTypes = propTypes;
RadioButtonGroup.defaultProps = defaultProps;
