import { StyleSheet, Dimensions } from "react-native";
import PlatformStyleSheet from "../../styles/PlatformStyleSheet";
import { window } from "../../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
export default PlatformStyleSheet.create({
  button: {
    margin: 15,
    marginBottom: 60,
    width: DeviceWidth - 30,
    height: 59,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#000000"
}

});
