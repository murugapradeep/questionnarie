import { StyleSheet } from "react-native";
import PlatformStyleSheet from "../../styles/PlatformStyleSheet";
import Theme from "../../helpers/Theme";
export default PlatformStyleSheet.create({
  progressBarBackground: {
    width: 100,
    height: 10,
    borderRadius : 5,
    backgroundColor: Theme.slider.background
  },
  progressBarHighlight: {
    width: 0,
    height: 10,
    borderRadius : 5,
    backgroundColor: Theme.slider.highlight
  },

});
