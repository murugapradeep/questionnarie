import React, { Component } from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import styles from "./styles";
import PropTypes from "prop-types";

const propTypes = {
  percent: PropTypes.number.isRequired,
  backgroundStyle: View.propTypes.style,
  highlightStyle: View.propTypes.style
};

const defaultProps = {
  percent: 0,
  backgroundStyle: {},
  highlightStyle: {}
};

export default class ProgressBar extends Component {
  constructor(props: Object) {
    super(props);
  }

  render() {
    return (
      <View style={[styles.progressBarBackground, this.props.backgroundStyle]}>
        <View style={[styles.progressBarHighlight, this.props.highlightStyle, {width: this.props.percent}]} />
      </View>
    );
  }
}
ProgressBar.propTypes = propTypes;
ProgressBar.defaultProps = defaultProps;
