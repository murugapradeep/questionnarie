import React, { Component } from "react";
import { View } from "react-native";
import styles from "./styles";
import PropTypes from "prop-types";

export default class PageControl extends Component {
  constructor(props: Object) {
    super(props);
  }
  render() {
    var pages = [];
    for (let i = 0; i < this.props.totalPages; i++) {
      pages.push(
        <View
          key={i}
          style={
            this.props.selIndex == i
              ? [styles.dot, styles.selectedDot]
              : styles.dot
          }
        />
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.pageControlView}>{pages}</View>
      </View>
    );
  }
}
