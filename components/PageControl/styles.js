import { StyleSheet, Dimensions } from "react-native";
import PlatformStyleSheet from "../../styles/PlatformStyleSheet";
import { window } from "../../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
export default PlatformStyleSheet.create({
  container:{
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
  },
  dot:{
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 3.5,
    width: 7,
    height: 7,
    backgroundColor: "rgba(0, 0, 0, 0.3)"
  },
  selectedDot:{
   backgroundColor: "#000000"
  },
  pageControlView:{
    flexDirection: "row",
  }

});
