import { StyleSheet, Dimensions } from "react-native";
import PlatformStyleSheet from "../../styles/PlatformStyleSheet";
import { window } from "../../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
export default PlatformStyleSheet.create({
  button: {
    margin: 15,
    marginBottom: 60,
    width: DeviceWidth - 30,
    height: 59,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#000000"
  },
  buttonText: {
    width: 50,
    height: 22,
    fontSize: 17,
    fontWeight: "bold",
    lineHeight: 22,
    textAlign: "center",
    color: "#ffffff"
  },
  messageText: {
    width: DeviceWidth,
    margin: 10,
    height: 60,
    fontSize: 26,
    lineHeight: 30,
    textAlign: "center",
    alignItems: 'center',
    color: "#000000"
  },
  selectedStyle: {
    fontSize: 17,
    fontWeight: "500",
    letterSpacing: -0.41,
    textAlign: "left",
    color: "#000000"
  },
  unselectedStyle: {
    fontSize: 17,
    letterSpacing: -0.41,
    textAlign: "left",
    color: "#4a4a4a"
  },
  freeTextTypeIn: {
    fontSize: 17,
    lineHeight: 26,
    letterSpacing: -0.21,
    textAlign: "left",
    color: "#000000"
  },
  questionTypeStyle: {
    fontSize: 17,
    lineHeight: 28,
    letterSpacing: -0.41,
    textAlign: "center",
    color: "#000000"
  }
});
