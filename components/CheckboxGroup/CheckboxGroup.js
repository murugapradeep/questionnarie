import React, { Component } from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import styles from "./styles";
import Checkbox from "../Checkbox";
import PropTypes from "prop-types";

const propTypes = {
  data: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  selectedIcon: Image.propTypes.source,
  unSelectedIcon: Image.propTypes.source,
  textStyle: Text.propTypes.style,
  selectedTextStyle: Text.propTypes.style
};

const defaultProps = {
  data: [],
  onChange: () => {},
  selectedIcon: require("../../images/multiSelectSelected.png"),
  unSelectedIcon: require("../../images/multiSelectUnselected.png"),
  textStyle: {},
  selectedTextStyle: {}
};

export default class CheckboxGroup extends Component {
  constructor(props: Object) {
    super(props);
    this.state = {
      selectedItems: []
    };
    this.options = props.data;
  }

  componentDidMount = () => {};

  _handleOnPress = idx => {
    var _selectedItems = this.state.selectedItems;
    if (_selectedItems.indexOf(idx) > -1) {
      _selectedItems.splice(_selectedItems.indexOf(idx), 1);
    } else {
      _selectedItems.push(idx);
    }
    this.setState({
      selectedItems: _selectedItems
    });
    this.props.onChange(_selectedItems);
  };

  _renderOptions = () => {};
  render() {
    const selectedIcon = this.props.selectedIcon;
    const unSelectedIcon = this.props.unSelectedIcon;
    return (
      <View style={{ flex: 1 }}>
        {this.options.map((option, i) => (
          <Checkbox
            key={i}
            image={
              this.state.selectedItems.indexOf(i) > -1
                ? selectedIcon
                : unSelectedIcon
            }
            iconType="image"
            title={option.title}
            isSelected={this.state.selectedItems.indexOf(i) > -1 ? true : false}
            onPress={this._handleOnPress.bind(this, i)}
            textStyle={this.props.textStyle}
            selectedTextStyle={this.props.selectedTextStyle}
          />
        ))}
      </View>
    );
  }
}
CheckboxGroup.propTypes = propTypes;
CheckboxGroup.defaultProps = defaultProps;
