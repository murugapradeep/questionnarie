import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import styles from "./styles";
import PropTypes from "prop-types";

const propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  style: View.propTypes.style,
  titleStyle: Text.propTypes.style,
};

const defaultProps = {
  title: "Submit",
  onPress: () => {},
  style: {},
  titleStyle: {},
};

export default class NavBarButton extends Component {
  constructor(props: Object) {
    super(props);
  }
  render() {
    return (
      <TouchableOpacity style={[styles.button, this.props.style]} onPress={this.props.onPress}>
        <Text style={[styles.buttonTitle, this.props.titleStyle]}>{this.props.title}</Text>
      </TouchableOpacity>
    );
  }
}

NavBarButton.propTypes = propTypes;
NavBarButton.defaultProps = defaultProps;
