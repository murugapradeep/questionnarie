import { StyleSheet, Dimensions } from "react-native";
import PlatformStyleSheet from "../../styles/PlatformStyleSheet";
import { window } from "../../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
import Theme from "../../helpers/Theme";
export default PlatformStyleSheet.create({
  button: {
    width: 80,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },
  buttonTitle: {
    fontSize: 17,
    fontWeight: "normal",
    lineHeight: 22,
    height: 20,
    textAlign: "center",
    color: Theme.button.background
  }
});
