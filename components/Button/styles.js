import { StyleSheet, Dimensions } from "react-native";
import PlatformStyleSheet from "../../styles/PlatformStyleSheet";
import { window } from "../../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
import Theme from "../../helpers/Theme";
export default PlatformStyleSheet.create({
  button: {
    margin: 15,
    marginBottom: 60,
    width: DeviceWidth - 30,
    height: 59,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Theme.button.background
  },
  buttonText: {
    width: 50,
    height: 22,
    fontSize: 17,
    fontWeight: "bold",
    lineHeight: 22,
    textAlign: "center",
    color: Theme.button.title
  }
});
