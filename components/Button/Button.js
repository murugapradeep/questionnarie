import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import styles from "./styles";
import PropTypes from "prop-types";

const propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  style: View.propTypes.style,
  titleStyle: Text.propTypes.style,
};

const defaultProps = {
  title: "Submit",
  onPress: () => {},
  style: {},
  titleStyle: {},
};

export default class Button extends Component {
  constructor(props: Object) {
    super(props);
  }
  render() {
    return (
      <TouchableOpacity style={[styles.button, this.props.style]} onPress={this.props.onPress}>
        <Text style={[styles.buttonText, this.props.buttonTitleStyle]}>{this.props.title}</Text>
      </TouchableOpacity>
    );
  }
}

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;
