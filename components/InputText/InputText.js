import React from "react";
import { Text, TouchableOpacity, TextInput } from "react-native";
import styles from "./styles";
const InputText = props => {
  return (
    <TextInput
      style={ props.multiline  ? styles.multilineInputField : styles.numberInputField}
      keyboardType="numeric"
      underlineColorAndroid={"transparent"}
      autoCapitalize="none"
      autoCorrect={false}
      multiline={props.multiline}
      numberOfLines={props.multiline ? 4 : 1}
      maxHeight={props.multiline ? 200 : 100}
      {...props}
    />
  );
};

export default InputText;
