import { StyleSheet, Dimensions } from "react-native";
import PlatformStyleSheet from "../../styles/PlatformStyleSheet";
import { window } from "../../helpers/Config";
import Theme from "../../helpers/Theme";
const { DeviceWidth, DeviceHeight } = window;

export default PlatformStyleSheet.create({
  numberInputField: {
    width: DeviceWidth-40,
    height: 65,
    borderRadius: 5,
    backgroundColor: Theme.textInput.background,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: Theme.textInput.border,
    fontSize: 24,
    textAlign: "center",
    color: Theme.textInput.text,
    marginTop: 20,
  },
  multilineInputField: {
    width: DeviceWidth-40,
    borderRadius: 5,
    backgroundColor:Theme.textInput.background,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: Theme.textInput.border,
    fontSize: 24,
    color: Theme.textInput.text,
    marginTop: 20,
    height: 200,
    textAlign: "left",
  }
});
