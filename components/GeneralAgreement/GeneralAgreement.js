import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import styles from "./styles";
import AgreementOption from "../AgreementOption";
import PropTypes from "prop-types";
const propTypes = {
  data: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  textStyle: Text.propTypes.style,
  selectedTextStyle: Text.propTypes.style,
};

const defaultProps = {
  data: [],
  onChange: () => {},
  textStyle: {},
  selectedTextStyle: {},
};
export default class GeneralAgreement extends Component {
  constructor(props: Object) {
    super(props);
    this.state = {
      selectedIndex: -1
    };
    this.options = props.data;
    //const { optionArray } = props;
  }

  componentDidMount = () => {};

  _handleOnPress = idx => {
    this.setState({
      selectedIndex: idx
    });
    this.props.onChange(idx);

  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.options.map((option, i) => (
          <AgreementOption
            key={i}
            image={
              this.state.selectedIndex == i
                ? option.selectedIcon
                : option.unSelectedIcon
            }
            iconType="image"
            title={option.title}
            isSelected={this.state.selectedIndex == i ? true : false}
            onPress={this._handleOnPress.bind(this, i)}
            textStyle={this.props.textStyle}
            selectedTextStyle={this.props.selectedTextStyle}
          />
        ))}
      </View>
    );
  }
}
GeneralAgreement.propTypes = propTypes;
GeneralAgreement.defaultProps = defaultProps;
