import React, { Component } from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import styles from "./styles";
import PropTypes from "prop-types";

const propTypes = {
  image: Image.propTypes.source.isRequired,
  title: PropTypes.string.isRequired,
  iconType: PropTypes.string,
  onPress: PropTypes.func.isRequired,
  isSelected: PropTypes.bool,
  textStyle: Text.propTypes.style,
  selectedTextStyle: Text.propTypes.style,
  iconStyle: Image.propTypes.style
};

const defaultProps = {
  title: "",
  iconType: "Image",
  onPress: () => {},
  isSelected: false,
  textStyle: {},
  selectedTextStyle: {}
};

export default class AgreementOption extends Component {
  constructor(props: Object) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity style={styles.row} onPress={this.props.onPress}>
        {this.props.iconType === "image" ? (
          <Image style={styles.icon} source={this.props.image} />
        ) : (
          <View style={style.circleView}>
            <Text style={style.circleLabelView}>1</Text>
          </View>
        )}

        <Text
          style={
            this.props.isSelected === false
              ? [styles.title, this.props.textStyle]
              : [
                  styles.title,
                  styles.selectedTitle,
                  this.props.selectedTextStyle
                ]
          }
        >
          {this.props.title}
        </Text>
      </TouchableOpacity>
    );
  }
}

AgreementOption.propTypes = propTypes;
AgreementOption.defaultProps = defaultProps;
