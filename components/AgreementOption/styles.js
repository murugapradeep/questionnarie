import { StyleSheet, Dimensions } from "react-native";
import PlatformStyleSheet from "../../styles/PlatformStyleSheet";
import { window } from "../../helpers/Config";
const { DeviceWidth, DeviceHeight } = window;
import Theme from "../../helpers/Theme";
export default PlatformStyleSheet.create({
  row: {
    flexDirection: "row",
    height: 55,
    width: DeviceWidth - 40,
    alignItems: "center",
    justifyContent: "center"
  },
  icon: {
    marginLeft: 20,
    width: 37,
    height: 37
  },
  title: {
    marginLeft: 10,
    width: 80,
    textAlign: "left",
    fontSize: 17,
    letterSpacing: -0.41,
    color: Theme.option.unSelectedText
  },
  selectedTitle: {
    fontSize: 17,
    fontWeight: "600",
    letterSpacing: -0.32,
    textAlign: "left",
    color: Theme.option.selectedText
  }
});
