import React, { Component } from "react";
import { View, Text } from "react-native";
import QuestionViewStyles from "../styles/QuestionViewStyles";
import Header from "../components/Header";
import GeneralAgreement from "../components/GeneralAgreement";
import ScrollViewWrapper from "../components/ScrollViewWrapper";
import { NavigationActions } from "react-navigation";
import PropTypes from "prop-types";
const propTypes = {
  data: PropTypes.array,
  onChange: PropTypes.func,
  initValue: PropTypes.string,
  pageNo: PropTypes.number,
  leftButtonTitle: PropTypes.string,
  rightButtonTitle: PropTypes.string,
  leftButtonHandler: PropTypes.func,
  rightButtonHandler: PropTypes.func,
  leftButtonStyle: View.propTypes.style,
  leftButtonTextStyle: Text.propTypes.style,
  rightButtonStyle: View.propTypes.style,
  rightButtonTextStyle: Text.propTypes.style,
  style: View.propTypes.style,
  headerStyle: View.propTypes.style,
  textStyle: Text.propTypes.style,
  selectedTextStyle: Text.propTypes.style
};

const defaultProps = {
  data: [],
  pageNo: 0,
  onChange: () => {},
  initValue: "Select me!",
  leftButtonTitle: "",
  rightButtonTitle: "",
  leftButtonHandler: () => {},
  rightButtonHandler: () => {},
  style: {},
  selectStyle: {},
  leftButtonStyle: {},
  leftButtonTextStyle: {},
  rightButtonStyle: {},
  rightButtonTextStyle: {},
  headerStyle: {},
  textStyle: {},
  selectedTextStyle: {}
};
const styles = QuestionViewStyles;
export default class GeneralOptionView extends Component {
  constructor(props, context) {
    super(props, context);
  }
  componentDidMount = () => {};

  handleOnValueChange = selectedValue => {
    console.log("val", selectedValue);
    this.props.onChange(selectedValue);
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          pageNo={this.props.pageNo}
          leftButtonTitle={this.props.leftButtonTitle}
          rightButtonTitle={this.props.rightButtonTitle}
          question={this.props.title}
          rightButtonHandler={this.props.rightButtonHandler}
          leftButtonHandler={this.props.leftButtonHandler}
          isNavBarHidden={false}
          headerStyle={this.props.headerStyle}
        />
        <View style={styles.answerView}>
          <ScrollViewWrapper>
            <GeneralAgreement
              data={this.props.data}
              textStyle={this.props.textStyle}
              selectedTextStyle={this.props.selectedTextStyle}
              onChange={this.handleOnValueChange}
            />
          </ScrollViewWrapper>
        </View>
      </View>
    );
  }
}

GeneralOptionView.propTypes = propTypes;
GeneralOptionView.defaultProps = defaultProps;
