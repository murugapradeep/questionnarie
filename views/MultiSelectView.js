import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import QuestionViewStyles from "../styles/QuestionViewStyles";
import Header from "../components/Header";
import CheckboxGroup from "../components/CheckboxGroup";
import ScrollViewWrapper from "../components/ScrollViewWrapper";
import PropTypes from "prop-types";
const propTypes = {
  data: PropTypes.array,
  onChange: PropTypes.func,
  initValue: PropTypes.string,
  pageNo: PropTypes.number,
  leftButtonTitle: PropTypes.string,
  rightButtonTitle: PropTypes.string,
  leftButtonHandler: PropTypes.func,
  rightButtonHandler: PropTypes.func,
  leftButtonStyle: View.propTypes.style,
  leftButtonTextStyle: Text.propTypes.style,
  rightButtonStyle: View.propTypes.style,
  rightButtonTextStyle: Text.propTypes.style,
  style: View.propTypes.style,
  headerStyle: View.propTypes.style,
  selectedIcon: Image.propTypes.source,
  unSelectedIcon: Image.propTypes.source,
  textStyle: Text.propTypes.style,
  selectedTextStyle: Text.propTypes.style
};

const defaultProps = {
  data: [],
  pageNo: 0,
  onChange: () => {},
  initValue: "Select me!",
  leftButtonTitle: "",
  rightButtonTitle: "",
  leftButtonHandler: () => {},
  rightButtonHandler: () => {},
  style: {},
  leftButtonStyle: {},
  leftButtonTextStyle: {},
  rightButtonStyle: {},
  rightButtonTextStyle: {},
  headerStyle: {},
  selectedIcon: require("../images/multiSelectSelected.png"),
  unSelectedIcon: require("../images/multiSelectUnselected.png"),
  textStyle: {},
  selectedTextStyle: {}
};
const styles = QuestionViewStyles;
export default class MultiSelectView extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount = () => {};

  handleOnValueChange = selectedValue => {
    console.log("val", selectedValue);
    this.props.onChange(selectedValue);
  };

  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <Header
          pageNo={this.props.pageNo}
          leftButtonTitle={this.props.leftButtonTitle}
          rightButtonTitle={this.props.rightButtonTitle}
          question={this.props.title}
          rightButtonHandler={this.props.rightButtonHandler}
          leftButtonHandler={this.props.leftButtonHandler}
          isNavBarHidden={false}
          headerStyle={this.props.headerStyle}
        />
        <View style={styles.answerView}>
          <ScrollViewWrapper>
            <CheckboxGroup
              data={this.props.data}
              selectedIcon={this.props.selectedIcon}
              unSelectedIcon={this.props.unSelectedIcon}
              textStyle={this.props.textStyle}
              selectedTextStyle={this.props.selectedTextStyle}
              onChange={this.handleOnValueChange}
            />
          </ScrollViewWrapper>
        </View>
      </View>
    );
  }
}
MultiSelectView.propTypes = propTypes;
MultiSelectView.defaultProps = defaultProps;
