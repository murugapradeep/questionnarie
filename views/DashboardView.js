import React, { Component } from "react";
import { View, Text } from "react-native";
import DashboardStyles from "../styles/DashboardStyles";
import Header from "../components/Header";
import DashboardRow from "../components/DashboardRow";
import ScrollViewWrapper from "../components/ScrollViewWrapper";
import PropTypes from "prop-types";

const propTypes = {
  data: PropTypes.array,
  title:PropTypes.string,
  onPress: PropTypes.func.isRequired,
  style: View.propTypes.style,
  titleStyle: Text.propTypes.style,
  dateStyle: Text.propTypes.style,
  statusStyle: Text.propTypes.style,
  stausBarView: View.propTypes.style,
  stausBarBackground: View.propTypes.style,
  stausBarHighlight: View.propTypes.style,
  rowStyle: View.propTypes.style,
  topView: View.propTypes.style,
  bottomView: View.propTypes.style
};

const defaultProps = {
  data: [],
  title: "",
  onPress: () => {},
  style: {},
  titleStyle: {},
  dateStyle: {},
  statusStyle: {},
  stausBarView: {},
  stausBarBackground: {},
  stausBarHighlight: {},
  rowStyle: {},
  topView: {},
  bottomView: {}
};
const styles = DashboardStyles;
export default class DashboardView extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount = () => {};

  handleOnValueChange = selectedValue => {
    console.log("val", selectedValue);
    this.props.onChange(selectedValue);
  };

  render() {
    return (
      <View style={[styles.container]}>
        <View style={styles.titleView}>
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
        <View style={styles.answerView}>
          <ScrollViewWrapper>
            {this.props.data.map((row, i) => (
              <DashboardRow
                key={i}
                title={row.title}
                percent={row.percent}
                date={row.date}
                status={row.status}
                onPress={this.props.onPress}
              />
            ))}
          </ScrollViewWrapper>
        </View>
      </View>
    );
  }
}
DashboardView.propTypes = propTypes;
DashboardView.defaultProps = defaultProps;
