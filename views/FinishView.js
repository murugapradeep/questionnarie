import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import FinishViewStyles from "../styles/FinishViewStyles";
import PropTypes from "prop-types";
import Header from "../components/Header";
const styles = FinishViewStyles;
const propTypes = {
  title: PropTypes.string,
  style: View.propTypes.style,
  titleStyle: Text.propTypes.style,
  message: PropTypes.string,
  messageStyle: Text.propTypes.style,
  closeIcon: Image.propTypes.source,
  closeButtonHandler: PropTypes.func,
  headerStyle: View.propTypes.style
};

const defaultProps = {
  title: "Submit",
  style: {},
  titleStyle: {},
  message: "",
  messageStyle: {},
  closeIcon: require("../images/close.png"),
  closeButtonHandler: () => {},
  headerStyle: {}
};
export default class FinishView extends Component {
  constructor(props, context) {
    super(props, context);
  }
  componentDidMount = () => {};

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.closeView}
          onPress={this.props.closeButtonHandler}
        >
          <Image style={styles.closeIcon} source={this.props.closeIcon} />
        </TouchableOpacity>
        <Header isNavBarHidden={true} headerStyle={{flex: 1}} />
        <View style={styles.instructionView}>
          <Text style={styles.messageTitle}>{this.props.title}</Text>
          <Text style={styles.messageBody}>{this.props.message}</Text>
        </View>
      </View>
    );
  }
}
FinishView.propTypes = propTypes;
FinishView.defaultProps = defaultProps;
