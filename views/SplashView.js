import React, { Component } from "react";
import { View, Text } from "react-native";
import SplashViewStyles from "../styles/SplashViewStyles";
import Button from "../components/Button";
import Header from "../components/Header";
import PropTypes from "prop-types";
const styles = SplashViewStyles;
const propTypes = {
  title: PropTypes.string,
  style: View.propTypes.style,
  titleStyle: Text.propTypes.style,
  okButtonTitle: PropTypes.string,
  okButtonHandler: PropTypes.func,
  okButtonStyle: View.propTypes.style,
  headerStyle: View.propTypes.style,
  okButtonTextStyle: Text.propTypes.style
};

const defaultProps = {
  title: "Submit",
  style: {},
  titleStyle: {},
  okButtonTitle: "",
  okButtonHandler: () => {},
  okButtonStyle: {},
  headerStyle: {},
  okButtonTextStyle: {}
};
export default class SplashView extends Component {
  constructor(props, context) {
    super(props, context);
  }
  componentDidMount = () => {};
  render() {
    return (
      <View style={styles.container}>
        <Header isNavBarHidden={true} headerStyle={{flex: 1}} />
        <View style={styles.instructionView}>
          <Text style={styles.instruction}>{this.props.title}</Text>
        </View>
        <Button title={this.props.okButtonTitle} onPress={this.props.okButtonHandler} />
      </View>
    );
  }
}
SplashView.propTypes = propTypes;
SplashView.defaultProps = defaultProps;
