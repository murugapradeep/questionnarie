import React, { Component } from "react";
import { View, Text } from "react-native";
import QuestionViewStyles from "../styles/QuestionViewStyles";
import Header from "../components/Header";
import InputText from "../components/InputText";
import ScrollViewWrapper from "../components/ScrollViewWrapper";
import PropTypes from "prop-types";
const propTypes = {
  onChange: PropTypes.func,
  initValue: PropTypes.string,
  pageNo: PropTypes.number,
  leftButtonTitle: PropTypes.string,
  rightButtonTitle: PropTypes.string,
  leftButtonHandler: PropTypes.func,
  rightButtonHandler: PropTypes.func,
  leftButtonStyle: View.propTypes.style,
  leftButtonTextStyle: Text.propTypes.style,
  rightButtonStyle: View.propTypes.style,
  rightButtonTextStyle: Text.propTypes.style,
  style: View.propTypes.style,
  headerStyle: View.propTypes.style
};

const defaultProps = {
  pageNo: 0,
  onChange: () => {},
  initValue: "",
  leftButtonTitle: "",
  rightButtonTitle: "",
  leftButtonHandler: () => {},
  rightButtonHandler: () => {},
  style: {},
  selectStyle: {},
  leftButtonStyle: {},
  leftButtonTextStyle: {},
  rightButtonStyle: {},
  rightButtonTextStyle: {},
  headerStyle: {}
};
const styles = QuestionViewStyles;
export default class MultiTextView extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount = () => {};

  handleOnValueChange = selectedValue => {
    console.log("val", selectedValue);
    this.props.onChange(selectedValue);
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          pageNo={this.props.pageNo}
          leftButtonTitle={this.props.leftButtonTitle}
          rightButtonTitle={this.props.rightButtonTitle}
          question={this.props.title}
          rightButtonHandler={this.props.rightButtonHandler}
          leftButtonHandler={this.props.leftButtonHandler}
          isNavBarHidden={false}
          headerStyle={this.props.headerStyle}
        />
        <View style={styles.answerView}>
          <ScrollViewWrapper>
            <InputText
              multiline={true}
              onChangeText={this.handleOnValueChange}
              value={this.props.initValue}
            />
          </ScrollViewWrapper>
        </View>
      </View>
    );
  }
}
MultiTextView.propTypes = propTypes;
MultiTextView.defaultProps = defaultProps;
