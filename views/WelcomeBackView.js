import React, { Component } from "react";
import { View, Text } from "react-native";
import SplashViewStyles from "../styles/SplashViewStyles";
import Button from "../components/Button";
import Header from "../components/Header";
import PropTypes from "prop-types";
const propTypes = {
  title: PropTypes.string,
  style: View.propTypes.style,
  titleStyle: Text.propTypes.style,
  okButtonTitle: PropTypes.string,
  okButtonHandler: PropTypes.func,
  okButtonStyle: View.propTypes.style,
  okButtonTextStyle: Text.propTypes.style,
  headerStyle: View.propTypes.style
};

const defaultProps = {
  title: "Submit",
  style: {},
  titleStyle: {},
  okButtonTitle: "",
  okButtonHandler: () => {},
  okButtonStyle: {},
  okButtonTextStyle: {},
  headerStyle: {}
};
const styles = SplashViewStyles;
export default class WelcomeBackView extends Component {
  constructor(props, context) {
    super(props, context);
  }
  componentDidMount = () => {};
  render() {
    return (
      <View style={styles.container}>
        <Header isNavBarHidden={true} headerStyle={{flex: 1}} />
        <View style={styles.instructionView}>
          <Text style={styles.instruction}>{this.props.title}</Text>
        </View>
        <Button title={this.props.okButtonTitle} onPress={this.props.okButtonHandler} />
      </View>
    );
  }
}
WelcomeBackView.propTypes = propTypes;
WelcomeBackView.defaultProps = defaultProps;
