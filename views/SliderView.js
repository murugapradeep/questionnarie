import React, { Component } from "react";
import { View, Text } from "react-native";
import QuestionViewStyles from "../styles/QuestionViewStyles";
import Header from "../components/Header";
import VerticalSlider from "../components/VerticalSlider";
import ScrollViewWrapper from "../components/ScrollViewWrapper";
import { window } from "../helpers/Config";
import PropTypes from "prop-types";
const { DeviceWidth, DeviceHeight } = window;
const styles = QuestionViewStyles;
const propTypes = {
  data: PropTypes.array,
  sliderLabels: PropTypes.array,
  onChange: PropTypes.func,
  initValue: PropTypes.number,
  pageNo: PropTypes.number,
  leftButtonTitle: PropTypes.string,
  rightButtonTitle: PropTypes.string,
  leftButtonHandler: PropTypes.func,
  rightButtonHandler: PropTypes.func,
  leftButtonStyle: View.propTypes.style,
  leftButtonTextStyle: Text.propTypes.style,
  rightButtonStyle: View.propTypes.style,
  rightButtonTextStyle: Text.propTypes.style,
  style: View.propTypes.style,
  headerStyle: View.propTypes.style
};

const defaultProps = {
  data: [],
  pageNo: 0,
  onChange: () => {},
  initValue: 0,
  leftButtonTitle: "",
  rightButtonTitle: "",
  leftButtonHandler: () => {},
  rightButtonHandler: () => {},
  style: {},
  selectStyle: {},
  leftButtonStyle: {},
  leftButtonTextStyle: {},
  rightButtonStyle: {},
  rightButtonTextStyle: {},
  headerStyle: {},
  sliderLabels: []
};
export default class SliderView extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      title: "",
      value: 0
    };
  }
  componentDidMount = () => {};

  render() {
    return (
      <View style={styles.container}>
        <Header
          pageNo={this.props.pageNo}
          leftButtonTitle={this.props.leftButtonTitle}
          rightButtonTitle={this.props.rightButtonTitle}
          question={this.props.title}
          rightButtonHandler={this.props.rightButtonHandler}
          leftButtonHandler={this.props.leftButtonHandler}
          isNavBarHidden={false}
          headerStyle={this.props.headerStyle}
        />
        <View style={styles.answerView}>
          <Text style={styles.sliderValue}>{this.state.value}</Text>
          <VerticalSlider
            value={this.state.value}
            onValueChange={val => {
              this.setState({ value: val });
              this.props.onChange(val);
            }}
          />
          <View style={styles.sliderLabelView}>
            {this.props.sliderLabels.map((option, i) => (
              <Text
                key={i}
                style={[
                  styles.sliderLabel,
                  { paddingTop: option.labelPosition }
                ]}
              >
                {option.title}
              </Text>
            ))}
          </View>
        </View>
      </View>
    );
  }
}
SliderView.propTypes = propTypes;
SliderView.defaultProps = defaultProps;
